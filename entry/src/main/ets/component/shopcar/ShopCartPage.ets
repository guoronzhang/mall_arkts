import { ShopCartConstants } from '../../constants/ShopCartConstants';
import { StyleConstants } from '../../constants/StyleConstants'
import { EmptyComponent } from '../EmptyComponent'
import { CounterProduct } from '../CounterProduct'
import { Product } from '../../viewmodel/ProductModel'
import { ShopCarEntity, ShopCartProduct } from '../../model/ShopCartModel'
import  ShopCartViewModel from '../../viewmodel/ShopCartViewModel'
import UserInfoTable, { UserInfoData } from '../../database/tables/UserInfoTable'
import Prompt from '@system.prompt';
import PrefUtil from '../../util/PrefUtil'


let viewModel = new ShopCartViewModel()

@Component
export struct ShopCartPage {
  @State sumPrice: number = 0;
  @State isSelectAll: boolean = false;
  @State showSettle: boolean = false;
  @State shopCarProductList: ShopCartProduct[] = []
  //登录后全局变量UserId会变化
  @StorageProp('UserId') @Watch('loginUserChanged') loginUserId: number = 0

  private userInfoTable: UserInfoTable = new UserInfoTable(()=>{})

  loginUserChanged(){
    this.loadCarList()
  }

  aboutToAppear(){
    this.loadCarList()
  }

  private loadCarList(){
    viewModel.loadShopCarList().then((list: ShopCarEntity[]) => {
      list.map(item => {
        let product = new ShopCartProduct(
          item.id,
          item.user_id,
          item.count,
          item.goods_id,
          item.order_id,
          item.goods_desc,
          item.goods_default_price,
          item.goods_default_icon
        )
        this.shopCarProductList.push(product)
      })
    })
  }

  /**
   * 结算
   */
  async settleAccounts(){
    try {
      let userId = await PrefUtil.getString('userId')
      if (userId.length !== 0) {
        this.userInfoTable.query(parseInt(userId), (result: UserInfoData[]) => {
          let userInfo: UserInfoData = result[0]
          userInfo.amount = userInfo.amount - this.sumPrice
          //更新本地数据库
          this.userInfoTable.updateData(userInfo, (resFlag: boolean) => {
            let msg = resFlag ? '扣款成功！' : '扣款失败！'
            Prompt.showToast({ message: msg, duration: 2000 })
            //删除列表数据
            if(resFlag){
              this.deletePaymentProducts()
            }
          })
        }, false)
      }
    } catch (e) {
      Prompt.showToast({ message: '扣款失败！', duration: 2000 })
    }
  }

  private async deletePaymentProducts(){
    let ids = ''
    this.shopCarProductList.map(item => {
      if(item.selected){
        ids = `${item.id},`
      }
    })
    //过滤未选中的商品
    this.shopCarProductList = this.shopCarProductList.filter(item => !item.selected)
    //删除后台数据
    ids = ids.substring(0, ids.lastIndexOf(','))
    await viewModel.deleteShopCar(ids)
    //重新拉取数据
    this.loadCarList()
  }

  //计算总价格
  private countSumPrice() {
    this.sumPrice = 0;
    this.isSelectAll = this.shopCarProductList.every((item: ShopCartProduct) => item.selected);
    let tempPrice: number = 0;
    this.shopCarProductList.forEach((item: ShopCartProduct) => {
      if (item.selected) {
        const ins = (item !== undefined ? item.goods_default_price * item.count : 0);
        tempPrice += ins;
      }
    })
    this.sumPrice = tempPrice;
  }

  private onItemSelectChange(index: number) {
    this.shopCarProductList[index].selected = !this.shopCarProductList[index].selected
    this.showSettle = this.shopCarProductList.some(item => item.selected)
    this.countSumPrice();
  }

  //设置选择状态值
  private selectProductChange(index: number): boolean {
    if (this.shopCarProductList[index] !== undefined) {
      return this.shopCarProductList[index].selected
    }
    return false;
  }

  //选择全部商品
  private selectAllProduct(select: boolean) {
    let selectArray: ShopCartProduct[] = []
    this.shopCarProductList.forEach(item => {
      item.selected = select
      selectArray.push(item)
    })
    this.shopCarProductList = selectArray
    this.showSettle = this.shopCarProductList.some(item => item.selected)
    this.countSumPrice()
  }

  //删除商品
  private deleteProduct(entity: ShopCartProduct) {
    this.shopCarProductList = this.shopCarProductList.filter(item => item.id !== entity.id)
    viewModel.deleteShopCar(`${entity.id}`).then((msg: string) =>  Prompt.showToast({message: msg, duration: 2000}))
  }

  @Builder
  ItemDelete(item: ShopCartProduct) {
    Flex({
      direction: FlexDirection.Column,
      justifyContent: FlexAlign.Center,
      alignItems: ItemAlign.End
    }) {
      Column() {
        Image($r('app.media.ic_trash'))
          .width($r('app.float.vp_twenty_four'))
          .height($r('app.float.vp_twenty_four'))
          .margin({ bottom: $r('app.float.vp_ten') })
        Text($r('app.string.delete'))
          .fontSize($r('app.float.small_font_size'))
          .fontColor(Color.White)
      }
      .padding({ right: $r('app.float.vp_fourteen') })
    }
    .onClick(() => {
      this.deleteProduct(item)
    })
    .height($r('app.float.item_delete_height'))
    .width($r('app.float.item_delete_width'))
    .backgroundColor($r('app.color.focus_color'))
    .borderRadius($r('app.float.vp_sixteen'))
    .margin({ left: $r('app.float.item_delete_margin_left') })
  }

  //商品布局组件
  @Builder
  CartItem(item: ShopCartProduct, index: number) {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
      //复选框
      Checkbox({
        name: `${ShopCartConstants.CHECKBOX}${index}`,
        group: ShopCartConstants.CHECKBOX_GROUP
      })
        .width($r('app.float.vp_twenty_four'))
        .height($r('app.float.vp_twenty_four'))
        .selectedColor($r('app.color.select_color'))
        .select(this.selectProductChange(index))
        .onClick(() => {
          this.onItemSelectChange(index)
        })

      Image(item.goods_default_icon)
        .height('100vp')
        .width('100vp')
        .objectFit(ImageFit.Cover)
        .margin({ left: $r('app.float.vp_sixteen') })

      Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.SpaceAround }) {
        //商品描述
        Text(item.goods_desc)
          .fontSize($r('app.float.small_font_size'))
          .margin({ bottom: $r('app.float.vp_eight') })
          .textOverflow({ overflow: TextOverflow.Ellipsis })
          .maxLines(StyleConstants.TWO_TEXT_LINE)
          .width(StyleConstants.FULL_WIDTH)

        Flex({ justifyContent: FlexAlign.SpaceBetween }) {
          //商品价格
          Text() {
            Span($r('app.string.rmb'))
              .fontSize($r('app.float.smaller_font_size'))
              .fontColor($r('app.color.focus_color'))
            Span(`${item.goods_default_price}`)
              .fontSize($r('app.float.middle_font_size'))
              .fontColor($r('app.color.focus_color'))
          }
          //商品数量
          CounterProduct({
            count: item.count,
            onNumberChange: (num: number) => {
              //同步更新量集合中的数量
              this.shopCarProductList[index].count = num
              this.countSumPrice()
            }
          })
        }
      }
      .margin({
        left: $r('app.float.vp_sixteen'),
        top: $r('app.float.vp_twelve'),
        bottom: $r('app.float.vp_twelve')
      })
      .width(StyleConstants.FULL_WIDTH)
    }
    .padding({
      left: $r('app.float.vp_twelve'),
      right: $r('app.float.vp_twelve')
    })
    .borderRadius($r('app.float.vp_sixteen'))
    .backgroundColor(Color.White)
    .width(StyleConstants.FULL_WIDTH)
    .height($r('app.float.item_delete_height'))
  }

  @Builder
  Settle() {
    Flex({ justifyContent: FlexAlign.SpaceBetween }) {
      Flex({ alignItems: ItemAlign.Center }) {
        //复选框
        Checkbox({ name: ShopCartConstants.CHECKBOX, group: ShopCartConstants.CHECKBOX_GROUP })
          .selectedColor($r('app.color.select_color'))
          .select(this.isSelectAll)
          .onClick(() => {
            this.isSelectAll = !this.isSelectAll;
            this.selectAllProduct(this.isSelectAll)
          })
        //选择全部文字
        Text($r('app.string.select_all'))
          .fontSize($r('app.float.small_font_size'))
          .fontColor(Color.Grey)
      }
      .height(StyleConstants.FULL_HEIGHT)
      .width($r('app.float.settle_select_width'))

      Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.End }) {
        Text($r('app.string.total'))
          .fontSize($r('app.float.smaller_font_size'))
          .fontWeight(FontWeight.Bold)
        Text() {
          Span($r('app.string.rmb'))
            .fontSize($r('app.float.smaller_font_size'))
            .fontColor($r('app.color.focus_color'))
          Span(`${this.sumPrice}`)
            .fontSize($r('app.float.middle_font_size'))
            .fontColor($r('app.color.focus_color'))
        }
        .margin({
          right: $r('app.float.vp_eight'),
          left: $r('app.float.vp_eight')
        })

        Button($r('app.string.checkout'), { type: ButtonType.Capsule, stateEffect: true })
          .backgroundColor($r('app.color.focus_color'))
          .fontSize($r('app.float.smaller_font_size'))
          .height($r('app.float.settle_button_height'))
          .width($r('app.float.settle_select_width'))
          .onClick(() => {
            this.settleAccounts();
          })
      }
      .height(StyleConstants.FULL_HEIGHT)
    }
    .padding({
      right: $r('app.float.vp_twelve'),
      left: $r('app.float.vp_twelve')
    })
    .backgroundColor(Color.White)
    .width(StyleConstants.FULL_WIDTH)
    .height($r('app.float.vp_fifty_six'))
  }

  build() {
    Flex({ direction: FlexDirection.Column }) {
      //页面标题
      Text($r('app.string.cart'))
        .fontSize($r('app.float.huge_font_size'))
        .fontColor(Color.White)
        .height($r('app.float.vp_fifty_six'))
        .padding({ left: $r('app.float.vp_twenty_four') })
        .width(StyleConstants.FULL_WIDTH)
        .textAlign(TextAlign.Center)
        .backgroundColor(0xFA2F22)

      //商品列表
      Scroll() {
        Column() {
          if (this.shopCarProductList.length > 0) {
            List({ space: StyleConstants.FIFTEEN_SPACE }) {
              ForEach(this.shopCarProductList, (item: ShopCartProduct, index?: number) => {
                ListItem() {
                  if (index !== undefined) {
                    this.CartItem(item, index)
                  }
                }
                .swipeAction({ end: this.ItemDelete(item) })
              }, (item: Product) => item.id)
            }
            .listDirection(Axis.Vertical)
          }
          //没数据UI
          else {
            Column() {
              EmptyComponent()
            }
            .width(StyleConstants.FULL_WIDTH)
            .height(StyleConstants.FULL_HEIGHT)
          }
        }
        .height(StyleConstants.FULL_HEIGHT)
      }
      .scrollBar(BarState.Off)
      .margin({
        left: $r('app.float.vp_twelve'),
        right: $r('app.float.vp_twelve')
      })
      .height(StyleConstants.FULL_HEIGHT)

      //全选布局
      if(this.showSettle){
        this.Settle()
      }
    }
    .width(StyleConstants.FULL_WIDTH)
    .backgroundColor($r('app.color.page_background'))
  }
}